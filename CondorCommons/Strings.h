#pragma once
#pragma once
#ifndef STRINGS_H
#define STRINGS_H
#include <string>
#include <vector>

namespace CondorCommons
{
	namespace Strings 
	{
		bool IsEmpty(const std::string &s);
		std::vector<std::string> BytesToStrings(std::vector<char> const &bytes);
		std::vector<char> StringsToBytes(std::vector<std::string> const &strings);

		class StringParser
		{
		public:
			StringParser(const std::string &source);
			StringParser(const std::vector<std::string> &source);
			~StringParser() {};
			std::string FirstParameter(const std::string &startMarker, const std::string &endMarker);
			std::string NextParameter(const std::string &startMarker, const std::string &endMarker);
			std::vector<std::string> AllParameters(const std::string &startMarker, const std::string &endMarker, bool distinct = false);
			std::vector<std::string> RemainingParameters(const std::string &startMarker, const std::string &endMarker, bool distinct = false);
			int CountParameters(const std::string &startMarker, const std::string &endMarker);
		private:
			std::string sourceString;
			std::vector<std::string> sourceVector;
			size_t currentPos;
			std::vector<std::string>::iterator iterator;

			void GotoStart();
		};

		//
		// Same as StringParser methods only as one function
		//
		inline std::string FirstParameter(const std::string &source, const std::string &startMarker, const std::string &endMarker)
		{
			return StringParser(source).FirstParameter(startMarker, endMarker);
		}
		inline std::string FirstParameter(const std::vector<std::string> &source, const std::string &startMarker, const std::string &endMarker)
		{
			return StringParser(source).FirstParameter(startMarker, endMarker);
		}

		inline std::vector<std::string> AllParameters(const std::string &source, const std::string &startMarker, const std::string &endMarker, bool distinct = false)
		{
			return StringParser(source).AllParameters(startMarker, endMarker, distinct);
		}
		inline std::vector<std::string> AllParameters(const std::vector<std::string> &source, const std::string &startMarker, const std::string &endMarker, bool distinct = false)
		{
			return StringParser(source).AllParameters(startMarker, endMarker, distinct);
		}
	}
}
#endif