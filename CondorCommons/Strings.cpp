#include "stdafx.h" 
#include <string>
#include <vector>
#include <sstream>
#include <array>
#include "IOHelper.h"
#include "Strings.h"

using namespace CondorCommons::IOHelper;
using namespace std;
 
//----------------------------------------------------------------------//
// StringParser
//----------------------------------------------------------------------//

namespace CondorCommons
{
	namespace Strings
	{
		//---------------------------------------------------------------//
		// Functions
		//---------------------------------------------------------------//
		bool IsEmpty(const string &s)
		{
			return s.length() == 0 || (nullptr == &s);
		}
		//
		// Converts Byte array to String array breaking up each line by either /r or /r/n
		//
		vector<string> BytesToStrings(vector<char> const &bytes)
		{
			vector<string> ret;
			if (bytes.size() > 0)
			{
				string endLine = "\n";
				// check if using \r\n
				auto pos = std::search(bytes.begin(), bytes.end(), endLine.begin(), endLine.end());
				if (pos != bytes.end() && pos != bytes.begin())
					if (*--pos == '\r')
						endLine = "\r\n";

				pos = bytes.begin();
				auto endLine_begin = endLine.begin();
				auto endLine_end = endLine.end();
				auto bytes_end = bytes.end();
				auto startPos = pos;
				int endLineCount = count(bytes.begin(), bytes_end, '\n');
				ret.reserve(endLineCount + 1);
				while (pos != bytes.end())
				{
					pos = std::search(pos, bytes_end, endLine_begin, endLine_end);
					ret.push_back(string(startPos, pos));
					if (pos != bytes.end())
					{
						pos += endLine.size();
						startPos = pos;
					}
				}
			}
			return ret;
		}

		//
		// Converts string array to byte array using std::endl as delimiter
		//
		vector<char> StringsToBytes(vector<string> const &strings)
		{
			if (strings.size() == 0)
				return vector<char>();

			string endLine;
			if (IsWindows())
				endLine = "\r\n";
			else
				endLine = "\n";

			stringstream sstream;
			copy(strings.begin(), strings.end(), ostream_iterator<string>(sstream, endLine.c_str()));

			std::string const& s = sstream.str();
			vector<char> ret; // (s.size());
			ret.reserve(s.size());
			ret.assign(s.begin(), s.end());
			return ret;
		}

		//---------------------------------------------------------------//
		// Classes
		//---------------------------------------------------------------//
		//
		// StringParser class
		//
		StringParser::StringParser(const string &source)
		{
			sourceVector.push_back(source);
			GotoStart();
		}

		StringParser::StringParser(const vector<string> &source)
		{
			sourceVector = source;
			GotoStart();
		}

		void StringParser::GotoStart()
		{
			iterator = sourceVector.begin();
			currentPos = 0;
			if (iterator >= sourceVector.end())
				sourceString = "";
			else
				sourceString = *iterator;
		}

		string StringParser::FirstParameter(const string &startMarker, const string &endMarker)
		{
			GotoStart();
			return NextParameter(startMarker, endMarker);
		}

		string StringParser::NextParameter(const string &startMarker, const string &endMarker)
		{
			if (currentPos >= sourceString.length() || currentPos == string::npos)
			{
				if (++iterator >= sourceVector.end())
					return "";
				else
				{
					sourceString = *iterator;
					currentPos = 0;
					return NextParameter(startMarker, endMarker);
				}
			}

			currentPos = IsEmpty(startMarker) ? currentPos : sourceString.find(startMarker, currentPos);
			if (string::npos == currentPos)
			{
				if (++iterator >= sourceVector.end())
					return "";
				else
				{
					sourceString = *iterator;
					currentPos = 0;
					return NextParameter(startMarker, endMarker);
				}
			}

			currentPos += startMarker.length();
			auto startPos = currentPos;

			currentPos = IsEmpty(endMarker) ? sourceString.length() : sourceString.find(endMarker, currentPos);
			if (string::npos == currentPos)
			{
				string str = sourceString.substr(startPos);
				return str + NextParameter("", endMarker);
			}

			auto ret = sourceString.substr(startPos, currentPos - startPos);
			currentPos += endMarker.length();
			return ret;
		}

		vector<string> StringParser::AllParameters(const string &startMarker, const string &endMarker, bool distinct)
		{
			vector<string> ret;
			auto temp = currentPos;
			string str = FirstParameter(startMarker, endMarker);
			while (!IsEmpty(str))
			{
				if (!distinct || std::find(ret.begin(), ret.end(), str) == ret.end())
					ret.push_back(str);
				str = NextParameter(startMarker, endMarker);
			}
			currentPos = temp;
			return ret;
		}

		vector<string> StringParser::RemainingParameters(const string &startMarker, const string &endMarker, bool distinct)
		{
			vector<string> ret;
			string str = NextParameter(startMarker, endMarker);
			while (!IsEmpty(str))
			{
				if (!distinct || std::find(ret.begin(), ret.end(), str) == ret.end())
					ret.push_back(str);
				str = NextParameter(startMarker, endMarker);
			}
			return ret;
		}

		int StringParser::CountParameters(const string &startMarker, const string &endMarker)
		{
			auto temp = currentPos;
			currentPos = 0;
			int count = 0;
			while (!IsEmpty(NextParameter(startMarker, endMarker)))
				count++;
			currentPos = temp;
			return count;
		}
	}
}