#include "stdafx.h" 
#include <chrono>
#include "Time.h"

using namespace std::chrono;

namespace CondorCommons
{
	namespace Time
	{
		int DaysFromBeginningOfTime(int year, int month, int day)
		{
			bool isLeap = year % 4 == 0 && year % 400 != 0;
			--year; // first year was year 1 - not 0
			int64_t days = year * 365 + year / 4 - year / 100 + year / 400;
			if (month > 1)
				for (int i = 0; i < month - 1; i++)
					days += monthDays[i];
			if (isLeap && month > 2)
				++days;
			return days + day;
		}

		bool IsLeapYear(int year)
		{
			return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
		}

		int64_t UtcOffsetSeconds()
		{
			struct tm gmTime;
			time_t localEpoch, gmEpoch;
			localEpoch = time(NULL);
			gmtime_s(&gmTime, &localEpoch);
			gmEpoch = mktime(&gmTime);

			int64_t diff = gmEpoch - localEpoch;
			return diff;
		}

		int64_t UtcOffsetTicks()
		{
			return UtcOffsetSeconds() * TicksPerSecond;
		}
		//-----------------------------------------------------------------------------//
		// Class StopWatch
		//-----------------------------------------------------------------------------//
		StopWatch::StopWatch()
		{
			Start();
		}

		void StopWatch::Start()
		{
			started = Clock::now();
			isPaused = false;
			pausedNanoseconds = 0;
		}

		void StopWatch::Pause()
		{
			paused = Clock::now();
			isPaused = true;
		}

		void StopWatch::Restart()
		{
			Start();
		}

		void StopWatch::Resume()
		{
			if (isPaused)
				pausedNanoseconds += duration_cast<nanoseconds>(paused - Clock::now()).count();
			isPaused = false;
		}

		int64_t StopWatch::ElapsedNanoseconds()
		{
			if (isPaused)
				return duration_cast<nanoseconds>(paused - started).count() - pausedNanoseconds;

			return duration_cast<nanoseconds>(Clock::now() - started).count() - pausedNanoseconds;
		}

		int64_t StopWatch::ElapsedTicks()
		{
			return ElapsedNanoseconds() / NanosecondsPerTick;
		}

		double StopWatch::ElapsedMicroseconds()
		{
			return (double)ElapsedTicks() / TicksPerMicrosecond;
		}

		double StopWatch::ElapsedMilliseconds()
		{
			return (double)ElapsedTicks() / TicksPerMillisecond;
		}

		double StopWatch::ElapsedSeconds()
		{
			return (double)ElapsedTicks() / TicksPerSecond;
		}

		double StopWatch::ElapsedMinutes()
		{
			return (double)ElapsedTicks() / TicksPerMinute;
		}

		double StopWatch::ElapsedHours()
		{
			return (double)ElapsedTicks() / TicksPerHour;
		}

		double StopWatch::ElapsedDays()
		{
			return (double)ElapsedTicks() / TicksPerDay;
		}

		//-----------------------------------------------------------------------------//
		// Class TimeSpan
		//-----------------------------------------------------------------------------//
		TimeSpan::TimeSpan(int days, int hours, int minutes, int seconds, int milliseconds, int microseconds) : Ticks(_Ticks)
		{
			_Ticks = (int64_t)days * TicksPerDay;
		}

		TimeSpan::TimeSpan(int64_t ticks) : Ticks(_Ticks)
		{
			_Ticks = ticks;
		}
		//
		// Statics to return TimeSpan object from a given time
		TimeSpan TimeSpan::FromHours(double value)
		{
			return TimeSpan((int64_t)(value * TicksPerHour));
		}
		TimeSpan TimeSpan::FromMinutes(double value)
		{
			return TimeSpan((int64_t)(value * TicksPerMinute));
		}
		TimeSpan TimeSpan::FromSeconds(double value)
		{
			return TimeSpan((int64_t)(value * TicksPerSecond));
		}
		TimeSpan TimeSpan::FromMilliseconds(double value)
		{
			return TimeSpan((int64_t)(value * TicksPerMillisecond));
		}
		TimeSpan TimeSpan::FromMicroseconds(double value)
		{
			return TimeSpan((int64_t)(value * TicksPerMicrosecond));
		}
		//
		// Total for entire TimeSpan
		int64_t TimeSpan::TotalNanoseconds()
		{
			return _Ticks * NanosecondsPerTick;
		}
		int64_t TimeSpan::TotalMicroseconds()
		{
			return _Ticks / TicksPerMicrosecond;
		}
		int64_t TimeSpan::TotalMilliseconds()
		{
			return _Ticks / TicksPerMillisecond;
		}
		double TimeSpan::TotalSeconds()
		{
			return (double)_Ticks / TicksPerSecond;
		}
		double TimeSpan::TotalMinutes()
		{
			return (double)_Ticks / TicksPerMinute;
		}
		double TimeSpan::TotalHours()
		{
			return (double)_Ticks / TicksPerHour;
		}
		double TimeSpan::TotalDays()
		{
			return (double)_Ticks / TicksPerDay;
		}
		//
		// Breakdown components of time interval
		int TimeSpan::Microseconds()
		{
			return (_Ticks % TicksPerMillisecond) / TicksPerMicrosecond;
		}
		int TimeSpan::Milliseconds()
		{
			return (_Ticks % TicksPerSecond) / TicksPerMillisecond;
		}
		int TimeSpan::Seconds()
		{
			return (_Ticks % TicksPerMinute) / TicksPerSecond;
		}
		int TimeSpan::Minutes()
		{
			return (_Ticks % TicksPerHour) / TicksPerMinute;
		}
		int TimeSpan::Hours()
		{
			return (_Ticks % TicksPerDay) / TicksPerHour;
		}
		int TimeSpan::Days()
		{
			return _Ticks / TicksPerDay;
		}


		//-----------------------------------------------------------------------------//
		// Class DateTime
		//-----------------------------------------------------------------------------//

		DateTime::DateTime(int year, int month, int day, DateTimeKind kind) : Ticks(_TicksSince1970)
		{
			DateTime(year, month, day, 0, 0, 0, 0, 0, kind);
		}

		DateTime::DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int microsecond, DateTimeKind kind) : Ticks(_TicksSince1970)
		{
			_kind = kind;
			int64_t offset = _kind == Local ? UtcOffsetTicks() : 0;
			_TicksSince1970 = (DaysFromBeginningOfTime(year, month, day) - _1970DaysFromBeginningOfTime) * TicksPerDay +
				((int64_t)hour * TicksPerHour) +
				((int64_t)minute * TicksPerMinute) +
				((int64_t)second * TicksPerSecond) +
				((int64_t)millisecond * TicksPerMillisecond) +
				((int64_t)microsecond * TicksPerMicrosecond) + offset; // Make sure ticks are in UTC
			CalculateDaysAndTimeOfDay();
		}

		DateTime::DateTime(int64_t UtcTicks, DateTimeKind kind) : Ticks(_TicksSince1970)
		{
			_kind = kind;
			_TicksSince1970 = UtcTicks;
			CalculateDaysAndTimeOfDay();
		}
		 
		DateTime DateTime::Date()
		{
			return DateTime(_DaysSince1970 * TicksPerDay, UTC);
		}

		void DateTime::SetKind(DateTimeKind kind)
		{
			_kind = kind;
			CalculateDaysAndTimeOfDay();
		}

		void DateTime::SetToNow()
		{
			_TicksSince1970 = UtcNow();
			CalculateDaysAndTimeOfDay();
		}

		DateTime DateTime::Today(DateTimeKind kind)
		{
			int64_t offset = kind == Local ? UtcOffsetTicks() : 0;
			auto now = UtcNow() - offset;
			return DateTime(now - (now % TicksPerDay));
		}
		
		int64_t DateTime::UtcNow()
		{
			auto now = std::chrono::system_clock::now();
			return std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count() / NanosecondsPerTick;
		}

		int DateTime::Day()
		{
			return _Day;
		}
		int DateTime::Month()
		{
			return _Month;
		}
		int DateTime::Year()
		{
			return _Year;
		}

		int DateTime::DayOfWeek()
		{
			auto day = (_DaysSince1970 + 4) % 7; // January 1 1970 was a Thursday
			return day < 0 ? day + 7 : day; 
		}
		int DateTime::DayOfYear()
		{
			return DaysFromBeginningOfTime(_Year, _Month, _Day) - DaysFromBeginningOfTime(_Year, 1, 1) + 1;
		}

		int DateTime::Hour()
		{
			return _TimeOfDayTicks / TicksPerHour;
		}
		int DateTime::Minute()
		{
			return (_TimeOfDayTicks % TicksPerHour) / TicksPerMinute;
		}
		int DateTime::Second()
		{
			return (_TimeOfDayTicks % TicksPerMinute) / TicksPerSecond;
		}
		int DateTime::Millisecond()
		{
			return (_TimeOfDayTicks % TicksPerSecond) / TicksPerMillisecond;
		}
		int DateTime::Microsecond()
		{
			return (_TimeOfDayTicks % TicksPerMillisecond) / TicksPerMicrosecond;
		}

		void DateTime::CalculateDaysAndTimeOfDay()
		{
			int64_t offset = _kind == Local ? UtcOffsetTicks() : 0;
			_DaysSince1970 = (_TicksSince1970 - offset) / TicksPerDay;
			_TimeOfDayTicks = (_TicksSince1970 - offset) % (_DaysSince1970 * TicksPerDay);

			auto daysLeft = _1970DaysFromBeginningOfTime + _DaysSince1970;
			// calculate year
			int y = 1;
			int leap = 0;
			while (daysLeft > (365 + leap))
			{
				daysLeft -= (365 + leap);
				++y;
				leap = IsLeapYear(y) ? 1 : 0;
			}
			_Year = y;

			// calculate month
			int m = 0;
			int mDays = monthDays[m];
			while (daysLeft > mDays)
			{
				daysLeft -= mDays;
				m++;
				if (m >= 12) // Shouldnt happen!
					break;
				mDays = monthDays[m] + (m == 2 ? leap : 0);
			}
			_Month = m + 1;
			_Day = daysLeft;
		}
	}
}