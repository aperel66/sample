#include "stdafx.h" 
#include <string>
#include <vector>
#include <fstream>
#include <stdio.h>
#include "IOHelper.h"
#include "Strings.h"

using namespace std;
using namespace CondorCommons::Strings; 

//----------------------------------------------------------------------//
// Helper functions for File Read/Writes
// Helper functions for determining current operating system
//----------------------------------------------------------------------//

namespace CondorCommons
{
	namespace IOHelper
	{
		//
		// Read All Lines from a text file into a vector of strings
		//
		vector<string> ReadAllLines(string fileName)
		{
			vector<char> bytes = ReadAllBytes(fileName);
			if (bytes.size() == 0)
				return vector<string>();

			return BytesToStrings(bytes);
		}

		//
		// Write All Lines to a text file from a vector of strings
		//
		bool WriteAllLines(string fileName, vector<string> const &contents)
		{
			return WriteAllBytes(fileName, StringsToBytes(contents));
		}

		//
		// Write All Lines to a text file from an array of strings
		//
		bool WriteAllLines(string fileName, string *contents, size_t contentSize)
		{
			vector<string> contentsV;
			for (size_t i = 0; i < contentSize; i++)
				contentsV.push_back(contents[i]);
			return WriteAllLines(fileName, contentsV);
		}

		//
		// Read All bytes of a binary file
		//
		vector<char> ReadAllBytes(string fileName)
		{
			ifstream inFile(fileName.c_str(), ios::binary | ios::ate);
			if (!inFile.is_open())
				return vector<char>(0);

			ifstream::pos_type length = inFile.tellg();
			vector<char> result(length);
			inFile.seekg(0, ios::beg);
			inFile.read(&result[0], length);
			inFile.close();
			return result;
		}

		//
		// Write All bytes to a binary file
		//
		bool WriteAllBytes(std::string fileName, std::vector<char> const &contents)
		{
			if (contents.size() == 0)
				return false;
			ofstream outFile(fileName.c_str(), ios::binary | ios::ate);
			if (!outFile.is_open())
				return false;
			outFile.write(&contents[0], contents.size());
			outFile.close();
			return true;
		}

		bool CopyFile(std::string sourceFileName, std::string destFileName)
		{
			return WriteAllBytes(destFileName, ReadAllBytes(sourceFileName));
		}

		//
		// Get OS Name
		//
		string GetOsName()
		{
#ifdef _WIN32
			return "Windows 32-bit";
#elif _WIN64
			return "Windows 64-bit";
#elif __unix || __unix__
			return "Unix";
#elif __APPLE__ || __MACH__
			return "Mac OSX";
#elif __linux__
			return "Linux";
#elif __FreeBSD__
			return "FreeBSD";
#else
			return "Other";
#endif // _WIN32
		}

		bool IsOS(string osName)
		{
			try
			{
				return osName.compare(GetOsName().substr(0, osName.size())) == 0;
			}
			catch (const std::exception e)
			{
				return false;
			}
		}
		//
		// Is it Windows?
		//
		bool IsWindows()
		{
			return IsOS("Windows");
		}

		//
		// Is it Linux (or Unix)
		//
		bool IsLinux()
		{
			return IsOS("Linux");
		}

		bool IsUnix()
		{
			return IsOS("Unix");
		}

		bool IsLinuxOrUnix()
		{
			return IsLinux() || IsUnix();
		}
	}
}