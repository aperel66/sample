#pragma once
#ifndef TIME_H
#define TIME_H
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;

namespace CondorCommons
{
	namespace Time
	{
		//
		// Enumeration
		enum DateTimeKind { Local, UTC };
		//
		// Supporting Functions
		int DaysFromBeginningOfTime(int year, int month, int day);
		bool IsLeapYear(int year);
		int64_t UtcOffsetSeconds();
		int64_t UtcOffsetTicks();
		//
		// Constants
		static const int NanosecondsPerTick = 100;
		static const int64_t TicksPerDay = 864000000000; // (int64_t)(24 * 60 * 60 * 1000 * 1000 * 10);
		static const int64_t TicksPerHour = 36000000000;
		static const int64_t TicksPerMinute = 600000000;
		static const int64_t TicksPerSecond = 10000000;
		static const int64_t TicksPerMillisecond = 10000;
		static const int64_t TicksPerMicrosecond = 10;
		static int monthDays[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		static int _1970DaysFromBeginningOfTime = DaysFromBeginningOfTime(1970, 1, 1);
		//
		// For Internal CPU timing
		//
		class StopWatch
		{
		public:
			StopWatch();
			void Start();
			void Pause();
			void Resume();
			void Restart();
			int64_t ElapsedNanoseconds();
			int64_t ElapsedTicks();
			double ElapsedMicroseconds();
			double ElapsedMilliseconds();
			double ElapsedSeconds();
			double ElapsedMinutes();
			double ElapsedHours();
			double ElapsedDays();
		private:
			std::chrono::time_point<std::chrono::steady_clock> started;
			std::chrono::time_point<std::chrono::steady_clock> paused;
			int64_t pausedNanoseconds;
			bool isPaused = false;
		};

		//
		// To Simplify DateTime operations - borrowed from C# - not a full port
		//
		class TimeSpan
		{
			int64_t _Ticks;
		public:
			TimeSpan() : Ticks(_Ticks) { _Ticks = 0; }; // initializes to 0
			TimeSpan(int days, int hours, int minutes, int seconds, int milliseconds = 0, int microseconds = 0);
			TimeSpan(int64_t ticks); // Initialize from ticks
			//
			// Overloads of Operators for TimeSpan
			TimeSpan operator+(const TimeSpan& t)
			{
				TimeSpan ret(this->Ticks + t.Ticks);
				return ret;
			}
			TimeSpan operator=(const TimeSpan& t)
			{
				return TimeSpan(t.Ticks);
			}
			TimeSpan operator-(const TimeSpan& t)
			{
				TimeSpan ret(this->Ticks - t.Ticks);
				return ret;
			}
			bool operator>(const TimeSpan& t)
			{
				return this->Ticks > t.Ticks;
			}
			bool operator>=(const TimeSpan& t)
			{
				return this->Ticks >= t.Ticks;
			}
			bool operator==(const TimeSpan& t)
			{
				return this->Ticks == t.Ticks;
			}
			bool operator<=(const TimeSpan& t)
			{
				return this->Ticks <= t.Ticks;
			}
			//
			// Statics to return TimeSpan object from a given time
			static TimeSpan FromHours(double value);
			static TimeSpan FromMinutes(double value);
			static TimeSpan FromSeconds(double value);
			static TimeSpan FromMilliseconds(double value);
			static TimeSpan FromMicroseconds(double value);
			//
			// Total for entire TimeSpan
			int64_t TotalNanoseconds();
			int64_t TotalMicroseconds();
			int64_t TotalMilliseconds();
			double TotalSeconds();
			double TotalMinutes();
			double TotalHours();
			double TotalDays();
			//
			// Breakdown components of time interval
			int Microseconds();
			int Milliseconds();
			int Minutes();
			int Seconds();
			int Hours();
			int Days();

			void SetTicks(int64_t ticks) { this->_Ticks = ticks; }
			const int64_t &Ticks;
		};
		//
		// To Simplify DateTime operations - borrowed from C# - not a full port
		//
		class DateTime
		{
			int64_t _TicksSince1970; // Always in UTC Time
			int _DaysSince1970; // In whatever kind is specified
			int _Year, _Month, _Day; // Calculated when _DaysSince1970 is calculated
			int64_t _TimeOfDayTicks; // In whatever kind is specified
			DateTimeKind _kind; // UTC or local
		public:
			DateTime(DateTimeKind kind = Local) : Ticks(_TicksSince1970) { _TicksSince1970 = 0; _kind = kind; }
			DateTime(int year, int month, int day, DateTimeKind = Local);
			DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond = 0, int microsecond = 0, DateTimeKind = Local);
			DateTime(int64_t UtcTicks, DateTimeKind = Local);

			//
			// Overloads of Operators for DateTime
			DateTime operator=(const DateTime& d)
			{
				return DateTime(d.Ticks, d._kind);
			}
			DateTime operator+(const TimeSpan& t)
			{
				DateTime ret(t.Ticks + this->Ticks);
				return ret;
			}
			TimeSpan operator-(const DateTime& d)
			{
				TimeSpan ret(this->Ticks - d.Ticks);
				return ret;
			}
			DateTime operator-(const TimeSpan& t)
			{
				DateTime ret(this->Ticks - t.Ticks);
				return ret;
			}
			bool operator==(const DateTime& d)
			{
				return this->Ticks == d.Ticks;
			}
			bool operator>=(const DateTime& d)
			{
				return this->Ticks >= d.Ticks;
			}
			bool operator>(const DateTime& d)
			{
				return this->Ticks > d.Ticks;
			}
			bool operator<=(const DateTime& d) 
			{
				return this->Ticks <= d.Ticks;
			}
			bool operator<(const DateTime& d)
			{
				return this->Ticks < d.Ticks;
			}
			//
			// Statics
			static DateTime Today(DateTimeKind = Local);
			static int64_t UtcNow();

			DateTime Date();
			void SetKind(DateTimeKind kind);
			void SetToNow();

			int Day();
			int Month();
			int Year();

			int DayOfWeek();
			int DayOfYear();

			int Hour();
			int Minute();
			int Second();
			int Millisecond();
			int Microsecond();

			int DaysSince1970() { return _DaysSince1970; }
			void SetTicks(int64_t ticks) { this->_TicksSince1970 = ticks; CalculateDaysAndTimeOfDay(); }
			const int64_t &Ticks;
		private:
			void CalculateDaysAndTimeOfDay();
		};
	}
}
#endif