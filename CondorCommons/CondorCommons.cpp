// CondorCommons.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "IOHelper.h"
#include "Time.h"
#include "Strings.h"
#include <ctime>
#include <iostream>
#include <chrono>
#include <algorithm>
using namespace std;
using namespace CondorCommons::IOHelper; 
using namespace CondorCommons::Time;
using namespace CondorCommons::Strings;

int main()
{
	StopWatch timer;
	//string sourceFile = "c:\\Temp\\Test.txt";
	//string destFile = "c:\\Temp\\Test2.txt";
	//CopyFile(sourceFile, destFile);
	//cout << "Fast Copy: " << timer.ElapsedMilliseconds() << " Milliseconds." << endl;
	//timer.Start();

	//vector<string> lines1 = ReadAllLines(sourceFile);
	//cout << "ReadAllLines Time is: " << timer.ElapsedMilliseconds() << " Milliseconds." << endl;
 //   WriteAllLines("c:\\Temp\\Test2.txt", lines1);
	//cout << "Copy Time is: " << timer.ElapsedMilliseconds() << " Milliseconds." << endl;

 //   vector<char> contents1 = ReadAllBytes("c:\\Temp\\Test.txt");
	//vector<char> contents2 = ReadAllBytes("c:\\Temp\\Test2.txt");

	//int64_t sum1 = 0, sum2 = 0;
	//timer.Start();
	//for (const char &c : contents1)
	//	sum1 += c;

	//for (const char &c : contents2)
	//	sum2 += c;

	//cout << "Compare sums 1: " << sum1 << " " << sum2 << (sum1 == sum2 ? " Yes" : " No") << " Elapsed Time: " << timer.ElapsedMicroseconds() << " Microseconds." << endl;
	//
	//timer.Start();
	//sum1 = 0;
	//sum2 = 0;
	//std::for_each(contents1.begin(), contents1.end(), [&sum1](char& c) { sum1 += c; });
	//std::for_each(contents2.begin(), contents2.end(), [&sum2](char& c) { sum2 += c; });
	//cout << "Compare sums 2: " << sum1 << " " << sum2 << (sum1 == sum2 ? " Yes" : " No") << " Elapsed Time: " << timer.ElapsedMicroseconds() << " Microseconds." << endl;

	std::chrono::time_point<std::chrono::system_clock> p1, earliest;
	p1 = std::chrono::system_clock::now();
	auto myrep = std::chrono::nanoseconds(p1 - earliest);
	
	auto years = std::chrono::duration_cast<std::chrono::hours>(p1 - earliest).count() / 24 / 365.25;
	auto microsec = std::chrono::duration_cast<std::chrono::microseconds>(p1 - earliest).count();
	auto nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(p1 - earliest).count();
	auto days = std::chrono::duration_cast<std::chrono::hours>(p1 - earliest).count() / 24;
	cout << "Days = " << days << endl;
	//DateTime dt(DateTime::UtcNow());
	DateTime dt(2016, 10, 11, 11, 25, 0);

	auto ratio = dt.DaysSince1970() / days;
	auto diff = dt.DaysSince1970() - days;
	auto hour = dt.Hour();
	auto minute = dt.Minute();
	auto second = dt.Second();
	auto weekday = dt.DayOfWeek();
	cout << "Days in DateTime class = " << dt.DaysSince1970() << "  " <<  ratio << "  " << diff
		<< "  Year: " << dt.Year() << "  Month: " << dt.Month() << "  Day: " << dt.Day()
		<< "  Hour: " << hour << "  Minute: " << minute << "  Second: " << second << "  Weekday: " << weekday << endl;

	auto d2 = dt + TimeSpan::FromSeconds(35);
	hour = d2.Hour();
	minute = d2.Minute();
	second = d2.Second();
	weekday = d2.DayOfWeek();
	cout << "New Date:  Year: " << d2.Year() << "  Month: " << d2.Month() << "  Day: " << d2.Day()
		<< "  Hour: " << hour << "  Minute: " << minute << "  Second: " << second << "  Weekday: " << weekday << endl;
	//timer.Start();
	//auto xml = ReadAllLines("c:\\Temp\\TestXml2.xml");
	//cout << "Read File: " << " Elapsed Time: " << timer.ElapsedMicroseconds() << " Microseconds." << endl;
	//timer.Start();
	//auto keywords = AllParameters(xml, "</", ">", true);
	//cout << "Get All Keywords: " << " Elapsed Time: " << timer.ElapsedMicroseconds() << " Microseconds." << endl;
	string temp;
	cin >> temp;
	return 0;
}

