#pragma once
#ifndef IOHELPER_H
#define IOHELPER_H
#include <string>
#include <vector>

namespace CondorCommons
{ 
	namespace IOHelper
	{
		std::vector<std::string> ReadAllLines(std::string fileName);
		bool WriteAllLines(std::string fileName, std::vector<std::string> const &contents);
		bool WriteAllLines(std::string fileName, std::string *contents, size_t contentSize);
		std::vector<char> ReadAllBytes(std::string fileName);
		bool WriteAllBytes(std::string fileName, std::vector<char> const &contents);
		bool CopyFile(std::string sourceFileName, std::string destFileName);
		std::string GetOsName();
		bool IsWindows();
		bool IsLinux();
		bool IsUnix();
		bool IsLinuxOrUnix();
	}
}
#endif // !IOHELPER_H
